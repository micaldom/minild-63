-- chikun :: 2015
-- Game flags


DEFAULT_SCALE = 1
FULLSCREEN    = false
GAME_VSYNC    = false
GAME_WIDTH    = 1280
GAME_HEIGHT   = 720
MIN_WIDTH     = 640
MIN_HEIGHT    = 360

IS_OUYA        = false  -- Whether or not the game is running on an OUYA
IS_PIXEL_BASED = false  -- Whether or not the game is pixel based


local current_dir = ...

-- Load test flags is they exist on the system
if (love.filesystem.exists(current_dir .. "/test_flags.lua")) then

	require(current_dir .. "test_flags")
end
