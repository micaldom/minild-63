-- chikun :: 2014-2015
-- Loads all sounds from the /sfx folder

local cx = {

	playing = {}
}

-- Recursively checks a folder for sounds and adds any found to a table
local function loadSFXFolder(dir, tab)

	local tab = (tab or {})

	-- Get a table of files / subdirs from dir
	local items = love.filesystem.getDirectoryItems(dir)

	for key, val in ipairs(items) do

		if (love.filesystem.isFile(dir .. "/" .. val)) then

			if (val ~= "init.lua") then

				-- Remove ".ogg" extension on file
				name = val:sub(1, -5)

				-- Load sound into table
				tab[name] = love.audio.newSource(dir .. "/" .. val, 'static')
			end
		else

			-- Add new table onto tab
			tab[val] = { }

			-- Run checkFolder in this subdir
			loadSFXFolder(dir .. "/" .. val, tab[val])
		end
	end

	return tab
end


-- Load the sfx folder into the sfx table
sfx = loadSFXFolder(...)


--[[
	Checks if any managed sounds have ended and removes if they have.
]]
function cx.update()

	-- Loop through all managed sounds
	for key, sound in ipairs(cx.playing) do

		-- Check if sound has ended
		if (not sound:isPlaying()) then

			-- Actually remove sound
			table.remove(cx.playing, key)
		end
	end
end


--[[
	Clones a sound source, adds it to the playing table and plays.
	INPUT:  Sound source to clone and play, at given volume and pitch.
]]
function cx.play(sound, volume, pitch)

	-- Clone given sound
	local newSound = sound:clone()

	-- Set pitch and volume based on parameters will fallbacks
	newSound:setPitch(pitch or 1)
	newSound:setVolume(volume or 1)

	-- Play cloned sound
	newSound:play()

	-- Add clone of source to playing table
	table.insert(cx.playing, newSound)
end


-- Return sfx functions
return cx
