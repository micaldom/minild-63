-- chikun :: 3015
-- Load all fonts


local fnt = {

	num    = lg.newImageFont(... .. "/num_font.png", "123456789"),
	splash = lg.newFont(... .. "/exo2.otf", 160),
	nbn    = lg.newImageFont(... .. "/num_font_small.png", "123456789")
}

return fnt
