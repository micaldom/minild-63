-- chikun :: 2014-2015
-- Miscellaneous functions


--[[
	Does a deep copy of a table.
	INPUT:  Table to copy.
	OUTPUT: Copied table.
]]
function deepCopy(orig)

	local orig_type = type(orig)
	local copy

	if (orig_type == "table") then

		copy = { }

		for orig_key, orig_value in next, orig, nil do

			copy[deepCopy(orig_key)] = deepCopy(orig_value)
		end

		setmetatable(copy, deepCopy(getmetatable(orig)))
	else

		-- If number, string, boolean, etc. do regular copy
		copy = orig
	end

	return copy
end
