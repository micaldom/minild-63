-- chikun :: 2014-2015
-- ThreeByThree state


-- Temporary state, removed at end of script
local ThreeByThreeState = class(PrototypeClass, function(new_class) end)


-- On state create
function ThreeByThreeState:create()

	self.player = {
		anim_step = 0,
		images = {
			gfx.player.still_1,
			gfx.player.still_2
		}
	}
end


-- On state update
function ThreeByThreeState:update(dt)

	self.player.anim_step = (self.player.anim_step + dt * 4) % 2

	if (self.transition) then

		self.transition.timer = self.transition.timer + dt * 4

		if (self.transition.timer >= 1) then

			GameController:setCurrent(self.transition.x, self.transition.y)

			self.transition = nil
		end
	else

		local x, y = GameController:getCurrent()

		local x_move, y_move =
			ci:getValue('right') - ci:getValue('left'),
			ci:getValue('down')  - ci:getValue('up')

		if ((x_move ~= 0 or y_move ~= 0) and
			(x + x_move > 0 and x + x_move < 10) and
			(y + y_move > 0 and y + y_move < 10)) then

			self.transition = {
				x = x + x_move,
				y = y + y_move,
				timer = 0
			}
		end

		if (ci:wasPressed('a')) then

			local square_x, square_y = GameController:getCurrent()

			if (not (GameController:isPermanent(square_x, square_y))) then
				cs:change('obo')
			end
		end

		if (ci:wasPressed('b')) then

			cs:change('nbn')
		end
	end
end


-- On state draw
function ThreeByThreeState:draw()

	lg.setColor(0, 0, 0)

	local non = GameController:getNon()

	local mod = {
		x = (non.x - 1) * 3,
		y = (non.y - 1) * 3
	}

	local origin, box_size =
		{ x = 265, y = 15 },
		230

	for x = 0, 2 do

		for y = 0, 2 do

			local pos = {
				x = x + mod.x + 1,
				y = y + mod.y + 1
			}

			local box_x, box_y, value =
				origin.x + box_size * x,
				origin.y + box_size * y,
				GameController:getString(pos.x, pos.y)

			if (GameController:isPermanent(pos.x, pos.y)) then

				lg.setColor(192, 192, 192)
				lg.rectangle('fill', box_x + 6, box_y + 6, 220, 220)
			end
			lg.setColor(0, 0, 0)
			lg.draw(gfx.box, box_x, box_y)

			lg.setFont(cf.num)
			lg.printf(value, box_x, box_y + 47, 230, 'center')
		end
	end

	local x, y = GameController:getCurrent()

	if (self.transition) then

		local timer = 1 - (math.cos(self.transition.timer * math.pi) + 1) / 2

		x = x + (self.transition.x - x) * timer
		y = y + (self.transition.y - y) * timer
	end

	x = origin.x + (x - mod.x - 1) * box_size
	y = origin.y + (y - mod.y - 1) * box_size

	lg.setColor(255, 255, 255)
	local frame = math.floor(self.player.anim_step + 1)
	cg.drawCentred(self.player.images[frame], x + 150, y + 180)
end


-- On state kill
function ThreeByThreeState:kill()

end


-- Transfer data to state loading script
return ThreeByThreeState
