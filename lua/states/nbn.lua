-- chikun :: 2014-2015
-- Nine by nine state


-- Temporary state, removed at end of script
local NineByNineState = class(PrototypeClass, function(new_class) end)


-- On state create
function NineByNineState:create()

	self.non = GameController:getNon()
end


-- On state update
function NineByNineState:update(dt)

	-- local movement variables
	local x_move, y_move = 0, 0

	-- Check if moving up or down
	if (ci:wasPressed('up')) then

		y_move = -1
	end
	if (ci:wasPressed('down')) then

		y_move = y_move + 1
	end

	-- Check if moving left or right
	if (ci:wasPressed('left')) then

		x_move = -1
	end
	if (ci:wasPressed('right')) then

		x_move = x_move + 1
	end

	-- workout new non
	local new_non = {
		x = math.clamp(1, self.non.x + x_move, 3),
		y = math.clamp(1, self.non.y + y_move, 3)
	}

	-- Make sure new nondrant is actually new
	if (self.non.x ~= new_non.x or self.non.y ~= new_non.y) then

		-- change current square to middle of new nondrant
		GameController.current_square[1] = new_non.x * 3 - 1
		GameController.current_square[2] = new_non.y * 3 - 1

		-- Get new nondrant
		self.non = GameController:getNon()
	end

	-- check if action button was pressed
	if (ci:wasPressed("a")) then

		-- change state to splash state
		cs:change("tbt")
	end

	-- check if back button was pressed
	if (ci:wasPressed("b")) then

		-- Workout first x and y positions
		local start_x, start_y =
			(self.non.x - 1) * 3 + 1,
			(self.non.y - 1) * 3 + 1

		-- For all x and y positions in quadrant
		for x = start_x, start_x + 3 do

			for y = start_y, start_y + 3 do

				-- If can change that position
				if (not GameController:isPermanent(x, y)) then

					-- Set that position to 0
					GameController:setValue(x, y, "0")
				end
			end
		end
	end
end


-- On state draw
function NineByNineState:draw()

	lg.setColor(0, 0, 0)

	-- set size of each individual box
	local box_size = math.floor(230 / 3)

	-- select font for drawing in each box
	lg.setFont(cf.nbn)

	-- Draw each little box
	for x = 0, 8 do

		for y = 0, 8 do

			-- Find coordinates and value of each box
			local origin_x, origin_y, value =
			    295 + box_size * x,
			    15  + box_size * y,
			    GameController:getString(x + 1, y + 1)

			if (GameController:isPermanent(x + 1, y + 1)) then

				lg.setColor(192, 192, 192)
				lg.rectangle('fill', origin_x, origin_y, box_size, box_size)
			end

			lg.setColor(0, 0, 0)

			-- Draw the box itself
			lg.rectangle('line', origin_x, origin_y, box_size, box_size)

			-- Write each value in square
			lg.printf(value, origin_x, origin_y + 10, box_size, 'center')
		end
	end

	-- Size of each non
	box_size = 228

	-- Change line width to 3
	lg.setLineWidth(3)

	-- Set color of each non to a grey
	lg.setColor(64, 64, 64)

	-- Draw each non
	for x = 0, 2 do

		for y = 0, 2 do

			local origin_x, origin_y =
				295 + box_size * x,
				15  + box_size * y

			lg.rectangle('line', origin_x, origin_y, box_size, box_size)
		end
	end

	-- Set color of current non to a red
	lg.setColor(255, 0, 0)

	-- Find where current non is
	local origin_x, origin_y =
		295 + box_size * (self.non.x - 1),
		15  + box_size * (self.non.y - 1)

	-- Draw current nondrant
	lg.rectangle('line', origin_x, origin_y, box_size, box_size)

	-- Reset line width to 1
	lg.setLineWidth(1)
end


-- On state kill
function NineByNineState:kill()

	self.non = nil
end


-- Transfer data to state loading script
return NineByNineState
