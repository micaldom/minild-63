-- chikun :: 2014-2015
-- OneByOne state


-- Temporary state, removed at end of script
local OneByOneState = class(PrototypeClass, function(new_class) end)


-- On state create
function OneByOneState:create()

	-- Find non of cell and set map based on that
	local non = GameController:getNon()
	cm.current = deepCopy(maps["level" .. (non.y - 1) * 3 + non.x])

	-- Create player
	self.player = {
		anim_step = 0,
		x = nil,
		y = nil,
		w = 64,
		h = 32,
		can_jump = true,
		y_move = 0,
		x_move = 0,
		images = {
			gfx.player.still_1,
			gfx.player.still_2
		}
	}
end


-- On state update
function OneByOneState:update(dt)

	-- animation for player object
	self.player.anim_step = (self.player.anim_step + dt * 4) % 2

	-- local variable to hold walls of the level
	local walls, ladders, hazards, doors  = nil, nil, nil, nil

	-- Iterate through all layers of the level
	for key, layer in ipairs(cm.current.layers) do

--		-- If layer is the player layer
--		if (layer.name == "player") then
--
--			-- check if player object doesn't have a y value
--			if (self.player.y == nil) then
--
--				-- set players x and y values to the player object
--				self.player.x = layer.objects[1].x
--				self.player.y = layer.objects[1].y
--			end
--
--			-- set layers player to same as the players
--			layer.objects[1].x = math.floor(self.player.x)
--			layer.objects[1].y = math.floor(self.player.y)
--		end

		-- If layer is the walls layer
		if (layer.name == "collision") then

			-- set walls table to be all walls in the level
			walls = layer.objects
		end

		-- If layer is the walls layer
		if (layer.name == "ladder") then

			-- set ladders table to be all ladder objects
			ladders = layer.objects
		end

		-- If the layer is the hazards layer
		if (layer.name == "hazard") then

			-- set hazards to be all hazard objects
			hazards = layer.objects
		end

		if (layer.name == "door") then

			doors = layer.objects
		end
	end

	for key, layer in ipairs(cm.current.layers) do

		if (layer.name == "player") then

			if (self.player.y == nil) then

				local square_x, square_y = GameController:getCurrent()
				local spawn_at = GameController:getValue(square_x, square_y)

				for key, door in ipairs(doors) do

					if (door.name == spawn_at) then

						self.player.x, self.player.y = door.x + 32, door.y + 32
					end
				end
			end
		end
	end

	-- If holding the jump button down
	if (ci:isDown("up")) then

		-- if able to jump
		if (self.player.can_jump == true) then

			self.player.can_jump = false
			self.player.y_move = -500

		end
	end

	-- Maximum possible speed in x direction
	local x_move_max = 600

	-- If holding right
	if (ci:isDown("right")) then

		-- Accelerate player
		self.player.x_move = self.player.x_move + 600 * dt

		if self.player.x_move > x_move_max then

			-- Limit maximum speed of player
			self.player.x_move = x_move_max
		end
	end

	-- If holding left
	if (ci:isDown("left")) then

		-- Accelerate player
		self.player.x_move = self.player.x_move - 600 * dt

		if self.player.x_move < -x_move_max then

			-- Limit maximum speed of player
			self.player.x_move = -x_move_max
		end
	end

	-- If neither left or right are down
	if (not (ci:isDown("right") or ci:isDown("left"))) then

		-- create a fake player
		local fake_player = {}

		-- temporary player to test ground
		fake_player.x = self.player.x
		fake_player.y = self.player.y + 1
		fake_player.w = self.player.w
		fake_player.h = self.player.h

		-- factor to vary speed reduction set to a quarter
		local reduction_factor = 0.25

		-- Makesure that walls do exist
		if (walls) then

			-- For every wall
			for key, wall in ipairs(walls) do

				-- Check if colliding with fake player
				if (self:checkCollisionRectangle(fake_player, wall)) then

					-- Check if wall is ice
					if wall.name == "ice" then

						-- Increase speed reduction to half
						reduction_factor = 0.5
					else

						-- Increase speed reduction to full
						reduction_factor = 1
					end
				end
			end
		end

		-- Decrease speed 10 times in relation to current speed
		for i = 1, 10 do
			self.player.x_move = self.player.x_move -
			                     (self.player.x_move * dt * reduction_factor)
		end
	end

	-- If trying to perform an action
	if (ci:isDown("a")) then

		-- Cycle through the layers
		for key, layer in ipairs(cm.current.layers) do

			-- If the layers name is door
			if (layer.name == "door") then

				-- For all doors in the level
				for key, door in ipairs(layer.objects) do

					-- Check collision with the player
					if self:checkCollisionRectangle(self.player, door) then

						-- Get local squares x and y values
						local square_x, square_y = GameController:getCurrent()

						-- Set current square to the doors name
						GameController:setValue(square_x, square_y, door.name)

						-- Check to see if win
						if (GameController:isFinished()) then
							love.event.quit()
						end

						-- change state to 3by3
						cs:change("tbt")
					end
				end
			end
		end
	end

	-- Move in x direction
	self.player.x = self.player.x + self.player.x_move * dt

	-- Make sure we have walls to check with beforehand
	if (walls) then

		-- Do same thing with all walls
		for key, wall in ipairs(walls) do

			if (self:checkCollisionRectangle(self.player, wall)) then
				-- reset players x to be outside of wall
				if (self.player.x > wall.x) then

					self.player.x = math.floor(self.player.x - self.player.x_move * dt)
				else

					self.player.x = math.ceil(self.player.x - self.player.x_move * dt)
				end

				self.player.x_move = 0
			end
		end
	end

	if (hazards) then

		for key, hazard in ipairs(hazards) do

			if (self:checkCollisionRectangle(self.player, hazard)) then

				local square_x, square_y = GameController:getCurrent()
				local respawn_at = GameController:getValue(square_x, square_y)

				for key, door in ipairs(doors) do

					if door.name == respawn_at then

						self.player.x = door.x + 32
						self.player.y = door.y + 32
						self.player.x_move = 0
						self.player.y_move = 0
					end
				end
			end
		end
	end

	if (ladders) then
		for key, ladder in ipairs(ladders) do
			if (self:checkCollisionRectangle(self.player, ladder)) then
				if (ci:isDown("up")) then
					self.player.y_move = -300
				else
					self.player.y_move = 0
				end
				if (ci:isDown("down")) then
					self.player.y_move = 300
				end
			end
		end
	end

	self.player.y = self.player.y + self.player.y_move * dt

	local collision = false
	-- Do same thing with all walls
	for key, wall in ipairs(walls) do


		if (self:checkCollisionRectangle(self.player, wall)) then
			-- reset players x to be outside of wall
			if self.player.y > wall.y then
				self.player.y = math.floor(self.player.y - self.player.y_move * dt)
			else
				self.player.y = math.ceil(self.player.y - self.player.y_move * dt)
				self.player.can_jump = true
			end

			self.player.y_move = 0
			collision = true
		end
	end

	if (not collision) then

		self.player.y_move = self.player.y_move + 1000 * dt
	end
end


-- On state draw
function OneByOneState:draw()

	local x, y =
		self.player.x or 0,
		self.player.y or 0

	lg.translate(math.floor(640 - x),
		         math.floor(360 - y))

	-- Draw current map
	lg.setColor(255, 255, 255)
	cm.draw()

	-- Draw foreground of maps
	cm.drawFG()

	for key,layer in ipairs(cm.current.layers) do

		if (layer.name == "door") then

			for key, door in ipairs(layer.objects) do

				lg.draw(gfx.door, door.x, door.y)
				lg.print(door.name, door.x, door.y)
			end
		end
	end

	local image_flip = 1
	local image_offset_x = 0

	if (self.player.x_move < 0) then

		image_flip = -1
		image_offset_x = 64
	end

	local frame = math.floor(self.player.anim_step + 1)
	lg.draw(self.player.images[frame], (self.player.x or 0), (self.player.y or 0) - 31, 0, image_flip,
			1, image_offset_x, image_offset_y)

	lg.origin()
end


-- On state kill
function OneByOneState:kill()

end


-- Will check if collision between two rectangular objects given
function OneByOneState:checkCollisionRectangle(obj1, obj2)

	-- If first objects x is smaller than second
	if (obj1.x <= obj2.x) then

		-- If first objects x plus width is greater then second objects x
		if(obj1.x + obj1.w > obj2.x) then

			-- If first objects y is smaller than second objects y
			if (obj1.y <= obj2.y) then

				-- If first objects y plus height is greater than second objects y
				if (obj1.y + obj1.h > obj2.y) then

					-- Means there is collision
					return(true)
				else

					-- Means no collision
					return(false)
				end
			-- If second objects y is smaller than first objects y
			elseif (obj2.y <= obj1.y) then

				-- If second objects y plus height is greater than first objects y
				if (obj2.y + obj2.h > obj1.y) then

					-- Means collision
					return(true)
				else

					-- Means no collision
					return(false)
				end
			end
		else

			-- Means no collision
			return(false)
		end

	-- If second objects x is smaller than first objects x
	elseif (obj2.x <= obj1.x) then

		-- If second objects x plus width is greater than first objects x
		if(obj2.x + obj2.w > obj1.x) then

			-- If first objects y is smaller than second objects y
			if (obj1.y <= obj2.y) then

				-- If first objects y plus height is greater than second objects y
				if (obj1.y + obj1.h > obj2.y) then

					-- Means collision
					return(true)
				else

					-- Means no collision
					return(false)
				end

			-- If second objects y is smaller than first objects y
			elseif (obj2.y <= obj1.y) then

				-- If second objects y plus height is greater than first objects y
				if (obj2.y + obj2.h > obj1.y) then

					-- Means collision
					return(true)
				else

					-- Means no collision
					return(false)
				end
			end
		else
			-- Means no collision
			return(false)
		end
	end
end


-- Transfer data to state loading script
return OneByOneState
