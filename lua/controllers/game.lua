GameController = {
	current_square = { 5, 5 },
	values = { },
	finish = { },
	permanent = { }
}


GameController.values = {
	{ "7", "3", "0", "4", "0", "2", "0", "6", "0" },
	{ "0", "9", "2", "8", "0", "0", "0", "0", "0" },
	{ "8", "0", "0", "0", "6", "0", "0", "0", "2" },
	{ "4", "6", "0", "0", "3", "0", "0", "8", "9" },
	{ "0", "0", "3", "0", "0", "0", "4", "0", "0" },
	{ "9", "1", "0", "0", "7", "0", "0", "5", "6" },
	{ "2", "0", "0", "0", "1", "0", "0", "0", "3" },
	{ "0", "0", "0", "0", "0", "7", "9", "4", "0" },
	{ "0", "7", "0", "3", "0", "9", "0", "2", "5" }
}

GameController.finish = {
	{ "7", "3", "1", "4", "9", "2", "5", "6", "8" },
	{ "6", "9", "2", "8", "5", "1", "7", "3", "4" },
	{ "8", "5", "4", "7", "6", "3", "1", "9", "2" },
	{ "4", "6", "7", "1", "3", "5", "2", "8", "9" },
	{ "5", "2", "3", "9", "8", "6", "4", "1", "7" },
	{ "9", "1", "8", "2", "7", "4", "3", "5", "6" },
	{ "2", "4", "9", "5", "1", "8", "6", "7", "3" },
	{ "3", "8", "5", "6", "2", "7", "9", "4", "1" },
	{ "1", "7", "6", "3", "4", "9", "8", "2", "5" }
}

-- Setting all the predetermined squares as permanent
for rkey, row in ipairs(GameController.values) do

	-- Create permanent row
	GameController.permanent[rkey] = { }
	for ckey, column in ipairs(row) do

		-- Create permanent column
		GameController.permanent[rkey][ckey] = { }

		-- Set permanent column to if the value in that position is not 0
		GameController.permanent[rkey][ckey] = (not (column == "0"))
	end
end


function GameController:update(dt)


end


function GameController:getNon()

	return {
		x = math.ceil(self.current_square[1] / 3),
		y = math.ceil(self.current_square[2] / 3)
	}
end


function GameController:getCurrent()

	return self.current_square[1], self.current_square[2]
end


function GameController:getString(x, y)

	local value = self:getValue(x, y)

	if (value == "0") then

		return ""
	else

		return tostring(value)
	end
end


function GameController:getValue(x, y)

	return self.values[y][x]
end


function GameController:isPermanent(x, y)

	return self.permanent[y][x]
end


-- Function to set value of a given position
-- Input:  X, Y, New Value
-- Output: New value in that position
function GameController:setValue(x, y, newValue)

	self.values[y][x] = newValue
end


function GameController:setCurrent(x, y)

	self.current_square = { x, y }
end


-- Function to test if the puzzle is solved
function GameController:isFinished()

	-- local variable that will see if puzzle is solved
	local finish = true

	for rkey, row in ipairs(self.values) do

		for ckey, column in ipairs(row) do

			-- Check if cell is equal to what it should be
			if (not (column == self.finish[rkey][ckey])) then

				finish = false
			end
		end
	end

	return(finish)
end
