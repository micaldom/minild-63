-- chikun :: 2015
-- Control scheme for keyboard

return {
	up    = { "key_w", "key_i", "key_up" },
	left  = { "key_a", "key_j", "key_left" },
	down  = { "key_s", "key_k", "key_down" },
	right = { "key_d", "key_l", "key_right" },
	a     = { "key_ ", "key_z", "key_return" },
	b     = { "key_x", "key_backspace", "key_lctrl" },
	start = { "key_c", "key_escape", "key_lshift" }
}
