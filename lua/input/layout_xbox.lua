-- chikun :: 2015
-- Control scheme for Xbox 360 controller

return {
	up    = { "pad_axis_lefty_neg", "pad_button_dpup" },
	left  = { "pad_axis_leftx_neg", "pad_button_dpleft" },
	down  = { "pad_axis_lefty_pos", "pad_button_dpdown" },
	right = { "pad_axis_leftx_pos", "pad_button_dpright" },
	a     = { "pad_button_a" },
	b     = { "pad_button_b" },
	start = { "pad_button_start" }
}
